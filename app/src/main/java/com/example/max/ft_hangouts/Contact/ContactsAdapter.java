package com.example.max.ft_hangouts.Contact;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.max.ft_hangouts.R;
import com.example.max.ft_hangouts.ViewContactActivity;

import java.util.List;

public class ContactsAdapter extends BaseAdapter {
    private Activity _activity;
    private List<Contact> _list;
    private LayoutInflater _inflater;

    public ContactsAdapter(Activity activity, List<Contact> data) {
        this._activity = activity;
        this._list = data;
        this._inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return this._list.size();
    }

    @Override
    public Object getItem(int position) {
        return this._list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = _inflater.inflate(R.layout.item_list_layout, parent, false);
        }

        final Contact contact = (Contact) getItem(position);

        TextView textView = (TextView) view.findViewById(R.id.item_list_lname);
        textView.setText(contact.get_last_name() + "  " + contact.get_name());

        ImageView imageView = (ImageView) view.findViewById(R.id.item_list_photo);
        String img_path = contact.get_photo();
        if (!img_path.isEmpty()) {
            imageView.setImageURI(Uri.parse(img_path));
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(_activity, ViewContactActivity.class);
                intent.putExtra("id", contact.get_id());
                _activity.startActivity(intent);
            }
        });

        return view;
    }
}
