package com.example.max.ft_hangouts;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.max.ft_hangouts.Contact.Contact;
import com.example.max.ft_hangouts.Contact.Sms;
import com.example.max.ft_hangouts.Contact.SmsAdapter;
import com.example.max.ft_hangouts.Storage.DataBase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

public class SmsActivity extends AppCompatActivity {
    private Contact     _contact;
    private List<Sms>   _sms;
    private SmsAdapter  _adapter;

    private EditText    _sms_text;
    private TextView    _contact_id;
    private ListView    _listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setStatusBarColor(ContextCompat.getColor(this, Theme.getColorId()));

        super.onCreate(savedInstanceState);
        setContentView(R.layout.sms_view);

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        _listView = (ListView) findViewById(R.id.sms_list);
        _sms_text = (EditText) findViewById(R.id.sms_text);
        _contact_id = (TextView) findViewById(R.id.contact_id);

        long id = 0;
        id = getIntent().getLongExtra("id", id);
        _contact = DataBase.getInstance(this).getContactById(id);

        _contact_id.setText(_contact.get_last_name() + "  " + _contact.get_name());

        _sms = parseSmsToList(_contact.get_sms());
        _adapter = new SmsAdapter(this, _sms);
        _listView.setAdapter(_adapter);

        // focus on last sms
        _listView.setSelection(_listView.getAdapter().getCount()-1);

    }

    static public List<Sms> parseSmsToList(String sms_json) {
        List<Sms> res = new ArrayList<>();

        if (sms_json == null || sms_json.isEmpty()) {
            return res;
        }

        try {
            JSONArray jsonArray = new JSONArray(sms_json);

            for (int i = 0; i < jsonArray.length(); ++i) {
                JSONObject obj = jsonArray.getJSONObject(i);
                Boolean owner = obj.getBoolean("owner");
                String msg = obj.getString("msg");

                res.add(new Sms(owner, msg));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return res;
    }

    static public String listToJson(List<Sms> data) {
        if (data == null || data.isEmpty()) {
            return "";
        }

        JSONArray array = new JSONArray();

        for (int i = 0; i < data.size(); ++i) {
            JSONObject sms = new JSONObject();

            try {
                sms.put("owner", data.get(i).get_owner());
                sms.put("msg", data.get(i).get_msg());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            array.put(sms);
        }

        return array.toString();
    }

    public void sendSms(View view) {
        SmsManager manager = SmsManager.getDefault();

        String message = _sms_text.getText().toString();
        int length = message.length();

        if(length > 160)
        {
            ArrayList<String> messageList = manager.divideMessage(message);
            manager.sendMultipartTextMessage(_contact.get_number(), null, messageList, null, null);
        } else {
            manager.sendTextMessage(_contact.get_number(), null, message, null, null);
        }

        Toast.makeText(this, R.string.sms_sended, Toast.LENGTH_SHORT).show();

        _sms.add(new Sms(true, message));
        _contact.set_sms(listToJson(_sms));
        DataBase.getInstance(this).updateContact(_contact);

        Intent intent = new Intent(this, ViewContactActivity.class);
        intent.putExtra("id", _contact.get_id());
        startActivity(intent);
    }

    public void removeSms(View view) {
        _contact.set_sms("");
        DataBase.getInstance(this).updateContact(_contact);

        Intent intent = new Intent(this, ViewContactActivity.class);
        intent.putExtra("id", _contact.get_id());
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, ViewContactActivity.class);
        intent.putExtra("id", _contact.get_id());
        startActivity(intent);
    }
}
