package com.example.max.ft_hangouts;

public class Theme {
    private static int _color_id;

    public static void setColorId(int id) {
        _color_id = id;
    }

    public static int getColorId() {
        return _color_id;
    }
}
