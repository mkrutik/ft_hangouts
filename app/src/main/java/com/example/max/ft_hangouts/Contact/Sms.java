package com.example.max.ft_hangouts.Contact;

public class Sms {
    private boolean _owner;
    private String _msg;

    public Sms(boolean owner, String msg) {
        this._owner = owner;
        this._msg = msg;
    }

    public boolean get_owner() {
        return _owner;
    }

    public String get_msg() {
        return _msg;
    }
}
