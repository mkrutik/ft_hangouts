package com.example.max.ft_hangouts.Contact;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.widget.Toast;

import com.example.max.ft_hangouts.Storage.DataBase;

import java.util.ArrayList;
import java.util.List;

import static com.example.max.ft_hangouts.SmsActivity.listToJson;
import static com.example.max.ft_hangouts.SmsActivity.parseSmsToList;

public class SmsReceiver extends BroadcastReceiver {

    private static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(SMS_RECEIVED)) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                // get sms objects
                Object[] pdus = (Object[]) bundle.get("pdus");
                if (pdus.length == 0) {
                    return;
                }
                // large message might be broken into many
                SmsMessage[] messages = new SmsMessage[pdus.length];
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < pdus.length; i++) {
                    messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                    sb.append(messages[i].getMessageBody());
                }
                String sender = messages[0].getOriginatingAddress();
                String message = sb.toString();

                saveMessage(context, sender, message);
            }
        }
    }

    private void saveMessage(Context context, String phone, String msg) {
        List<Contact> contacts = DataBase.getInstance(context).getContacts();

        String number = phone;

        String remove = "+380";
        if (number.startsWith(remove)){
            number = number.substring(remove.length());
        }

        Contact res = null;

        for (Contact contact : contacts) {
            if (contact.get_number().contains(number)){
                res = contact;
                break;
            }
        }

        if (res == null) {
            List<Sms> tmp = new ArrayList<>();
            tmp.add(new Sms(false, msg));

            res = new Contact(0, phone, phone, phone, "", "", "", listToJson(tmp));
            DataBase.getInstance(context).addNewContact(res);
        } else {
            List<Sms> tmp = parseSmsToList(res.get_sms());
            tmp.add(new Sms(false, msg));
            res.set_sms(listToJson(tmp));

            DataBase.getInstance(context).updateContact(res);
        }
    }
}