package com.example.max.ft_hangouts.Contact;

public class Contact {
    private long    _id;
    private String _last_name;
    private String _address;
    private String _number;
    private String _name;
    private String _email;
    private String _photo;
    private String _sms;

    public Contact() {
        this._id = 0;
        this._last_name = "";
        this._name = "";
        this._address = "";
        this._number = "";
        this._email = "";
        this._photo = "";
        this._sms = "";
    }

    public Contact(long _id, String _last_name, String _name, String _number, String _address, String _email, String photo, String sms) {
        this._id = _id;
        this._last_name = _last_name;
        this._name = _name;
        this._address = _address;
        this._number = _number;
        this._email = _email;
        this._photo = photo;
        this._sms = sms;
    }

    public void set_photo(String _photo) {
        this._photo = _photo;
    }

    public String get_sms() {
        return _sms;
    }

    public String get_photo() {
        return _photo;
    }

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public String get_last_name() {
        return _last_name;
    }

    public String get_name() {
        return _name;
    }

    public String get_adress() {
        return _address;
    }

    public String get_number() {
        return _number;
    }

    public String get_email() {
        return _email;
    }

    public void set_last_name(String _last_name) {
        this._last_name = _last_name;
    }

    public void set_address(String _address) {
        this._address = _address;
    }

    public void set_number(String _number) {
        this._number = _number;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public void set_email(String _email) {
        this._email = _email;
    }

    public void set_sms(String _sms) {
        this._sms = _sms;
    }
}
