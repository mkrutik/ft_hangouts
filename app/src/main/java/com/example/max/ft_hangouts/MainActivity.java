package com.example.max.ft_hangouts;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.SearchView;

import com.example.max.ft_hangouts.Contact.Contact;
import com.example.max.ft_hangouts.Contact.ContactsAdapter;
import com.example.max.ft_hangouts.Storage.DataBase;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private ListView        _listView;
    private ContactsAdapter _adapter;
    private List<Contact>   _data;

    private static boolean _is_checked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (_is_checked == false) {
            Theme.setColorId(R.color.colorPrimary);
        }
        getWindow().setStatusBarColor(ContextCompat.getColor(this, Theme.getColorId()));

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        _listView = (ListView) findViewById(R.id.list_view);

        _data = DataBase.getInstance(this).getContacts();
        _adapter = new ContactsAdapter(this, _data);
        _listView.setAdapter(_adapter);
    }

    public void addContact(View view) {
        Intent intent = new Intent(this, EditeContactActivity.class);
        intent.putExtra("empty", true);

        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);

        MenuItem search_item = menu.findItem(R.id.app_bar_search);
        final SearchView searchView = (SearchView) search_item.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Context context = getApplicationContext();

                if (newText != "") {
                    _data.clear();
                    _data.addAll(DataBase.getInstance(context).search(newText));
                }

                _adapter.notifyDataSetChanged();

                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.change_color) {
            if (_is_checked) {
                Theme.setColorId(R.color.colorPrimary);
                _is_checked = false;
            } else {
                Theme.setColorId(R.color.colorAccent);
                _is_checked = true;
            }

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }
}
