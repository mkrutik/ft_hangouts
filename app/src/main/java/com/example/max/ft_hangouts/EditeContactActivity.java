package com.example.max.ft_hangouts;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.max.ft_hangouts.Contact.Contact;
import com.example.max.ft_hangouts.Storage.DataBase;

public class EditeContactActivity extends AppCompatActivity {
    private EditText _last_name;
    private EditText _name;
    private EditText _number;
    private EditText _address;
    private EditText _email;
    private ImageView _photo;

    private Contact _contact;

    private final int FROM_GALLERY = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setStatusBarColor(ContextCompat.getColor(this, Theme.getColorId()));

        super.onCreate(savedInstanceState);
        setContentView(R.layout.edite_contact_layout);

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        _last_name = (EditText) findViewById(R.id.last_name);
        _name = (EditText) findViewById(R.id.name);
        _number = (EditText) findViewById(R.id.number);
        _address = (EditText) findViewById(R.id.address);
        _email = (EditText) findViewById(R.id.email);
        _photo = (ImageView) findViewById(R.id.avatar);


        boolean empty = true;
        empty = getIntent().getBooleanExtra("empty", empty);
        if (!empty) {
            long id = 0;
            id = getIntent().getLongExtra("id", id);
            _contact = DataBase.getInstance(this).getContactById(id);

            _last_name.setText(_contact.get_last_name());
            _name.setText(_contact.get_name());
            _number.setText(_contact.get_number());
            _address.setText(_contact.get_adress());
            _email.setText(_contact.get_email());

            if (!_contact.get_photo().isEmpty()) {
                _photo.setImageURI(Uri.parse(_contact.get_photo()));
            }

        } else {
            _contact = new Contact();
        }
    }

    public void pickPhoto(View view) {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto , FROM_GALLERY);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
            if (requestCode == FROM_GALLERY && resultCode == RESULT_OK) {
                Uri selectedImage = imageReturnedIntent.getData();
                _contact.set_photo(selectedImage.toString());

                _photo.setImageURI(selectedImage);
        }
    }

    public void saveContact(View view) {
        _contact.set_last_name(_last_name.getText().toString());
        _contact.set_name(_name.getText().toString());
        _contact.set_number(_number.getText().toString());
        _contact.set_address(_address.getText().toString());
        _contact.set_email(_email.getText().toString());

        if (_contact.get_last_name().isEmpty() ||
                _contact.get_name().isEmpty() ||
                _contact.get_number().isEmpty()) {
            Toast.makeText(getApplicationContext(), R.string.fill, Toast.LENGTH_LONG).show();
            return;
        }

        if (_contact.get_id() == 0) {
            _contact.set_id(DataBase.getInstance(this).addNewContact(_contact));
            _contact.set_sms("");
            Toast.makeText(getApplicationContext(), R.string.contact_created, Toast.LENGTH_SHORT).show();
        } else {
            DataBase.getInstance(this).updateContact(_contact);
            Toast.makeText(getApplicationContext(), R.string.contact_updated, Toast.LENGTH_SHORT).show();
        }


        Intent intent = new Intent(this, ViewContactActivity.class);
        intent.putExtra("id", _contact.get_id());
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, ViewContactActivity.class);
        intent.putExtra("id", _contact.get_id());
        startActivity(intent);
    }
}
