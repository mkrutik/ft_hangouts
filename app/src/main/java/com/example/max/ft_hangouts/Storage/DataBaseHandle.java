package com.example.max.ft_hangouts.Storage;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHandle extends SQLiteOpenHelper {
    // Config
    final static private String DATA_BASE_NAME = "ft_hangouts.db";
    final static private int VERSION = 1;

    final static String TABLE = "contacts";
    // Column
    final static String ID = "id";
    final static String LAST_NAME = "last_name";
    final static String NAME = "name";
    final static String NUMBER = "number";
    final static String EMAIL = "email";
    final static String ADDRESS = "address";
    final static String PHOTO = "photo";
    final static String SMS = "sms";

    private static DataBaseHandle _instance;

    public static synchronized DataBaseHandle getInstance(Context context) {
        if (_instance == null) {
            _instance = new DataBaseHandle(context.getApplicationContext());
        }
        return _instance;
    }

    private DataBaseHandle(Context context) {
        super(context, DATA_BASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        final String query = "CREATE TABLE IF NOT EXISTS " + TABLE + " ( "
                + ID + " INTEGER PRIMARY KEY , "
                + LAST_NAME + " TEXT , "
                + NAME + " TEXT , "
                + NUMBER + " TEXT , "
                + EMAIL + " TEXT , "
                + ADDRESS + " TEXT , "
                + PHOTO + " TEXT , "
                + SMS + " TEXT "
                + " );";

        sqLiteDatabase.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE + ";");
        onCreate(db);
    }
}
