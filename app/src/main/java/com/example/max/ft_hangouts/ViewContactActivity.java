package com.example.max.ft_hangouts;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.max.ft_hangouts.Contact.Contact;
import com.example.max.ft_hangouts.Storage.DataBase;

public class ViewContactActivity extends AppCompatActivity {
    private TextView _last_name;
    private TextView _name;
    private TextView _number;
    private TextView _adrress;
    private TextView _email;
    private ImageView _photo;

    private Contact _contact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setStatusBarColor(ContextCompat.getColor(this, Theme.getColorId()));

        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_activity_contact);

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        _last_name = (TextView) findViewById(R.id.last_name);
        _name = (TextView) findViewById(R.id.name);
        _number = (TextView) findViewById(R.id.number);
        _adrress = (TextView) findViewById(R.id.address);
        _email = (TextView) findViewById(R.id.email);
        _photo = (ImageView) findViewById(R.id.avatar);

        long id = 0;
        id = getIntent().getLongExtra("id", id);
        _contact = DataBase.getInstance(this).getContactById(id);

        _last_name.setText(_contact.get_last_name());
        _name.setText(_contact.get_name());
        _number.setText(_contact.get_number());
        _adrress.setText(_contact.get_adress());
        _email.setText(_contact.get_email());

        if (!_contact.get_photo().isEmpty()) {
            _photo.setImageURI(Uri.parse(_contact.get_photo()));
        }
    }

    public void deleteContact(View view) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(R.string.delete_title);
        alertDialog.setMessage(R.string.delete);
        alertDialog.setIcon(R.mipmap.delete);

        alertDialog.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(), R.string.contact_removed, Toast.LENGTH_SHORT).show();
                dialog.cancel();

                DataBase.getInstance(getApplicationContext()).removeContact(_contact);

                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });

        alertDialog.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }

    public void editContact(View view) {
        Intent intent = new Intent(this, EditeContactActivity.class);
        intent.putExtra("empty", false);
        intent.putExtra("id", _contact.get_id());
        startActivity(intent);
    }

    public void callNumber(View view) {
        Intent dialIntent = new Intent(Intent.ACTION_CALL);
        dialIntent.setData(Uri.parse("tel:" + _contact.get_number()));

        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        if (dialIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(dialIntent);
        }
    }

    public void smsView(View view) {
        Intent intent = new Intent(this, SmsActivity.class);
        intent.putExtra("id", _contact.get_id());
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}