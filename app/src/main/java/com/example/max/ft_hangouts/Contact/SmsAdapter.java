package com.example.max.ft_hangouts.Contact;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.max.ft_hangouts.R;

import java.util.List;

public class SmsAdapter extends BaseAdapter {
    private Activity _activity;
    private List<Sms> _list;
    private LayoutInflater _inflater;

    public SmsAdapter(Activity activity, List<Sms> data) {
        this._activity = activity;
        this._list = data;
        this._inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return _list.size();
    }

    @Override
    public Object getItem(int position) {
        return _list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = _inflater.inflate(R.layout.sms_item, parent, false);
        }

        final Sms sms = (Sms) getItem(position);

        TextView to_my = (TextView) view.findViewById(R.id.to_me);
        TextView my = (TextView) view.findViewById(R.id.my_sms);

        if (sms.get_owner()) {
            my.setText(sms.get_msg());
        } else {
            to_my.setText(sms.get_msg());
        }

        return view;    }
}
