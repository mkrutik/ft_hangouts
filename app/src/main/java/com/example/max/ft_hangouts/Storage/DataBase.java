package com.example.max.ft_hangouts.Storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.max.ft_hangouts.Contact.Contact;

import java.util.ArrayList;
import java.util.List;

import static com.example.max.ft_hangouts.Storage.DataBaseHandle.ADDRESS;
import static com.example.max.ft_hangouts.Storage.DataBaseHandle.EMAIL;
import static com.example.max.ft_hangouts.Storage.DataBaseHandle.ID;
import static com.example.max.ft_hangouts.Storage.DataBaseHandle.LAST_NAME;
import static com.example.max.ft_hangouts.Storage.DataBaseHandle.NAME;
import static com.example.max.ft_hangouts.Storage.DataBaseHandle.NUMBER;
import static com.example.max.ft_hangouts.Storage.DataBaseHandle.PHOTO;
import static com.example.max.ft_hangouts.Storage.DataBaseHandle.SMS;
import static com.example.max.ft_hangouts.Storage.DataBaseHandle.TABLE;

public class DataBase {
    private static DataBase         _dataBase;
    private static SQLiteDatabase   _db;
//    private static List<Contact>    _cache = new ArrayList<Contact>();

    public static synchronized DataBase getInstance(Context context) {
        if (_dataBase == null) {
            _dataBase = new DataBase(context.getApplicationContext());
        }

        return _dataBase;
    }

    private DataBase(Context context){
        _db = DataBaseHandle.getInstance(context).getWritableDatabase();
    }

    public List<Contact> getContacts() {
        List<Contact> res = new ArrayList<>();
            final String query = "SELECT * FROM " + TABLE + ";";
            Cursor cursor = _db.rawQuery(query, null);

            if (cursor != null && cursor.moveToFirst()) {
                do {
                    int id = cursor.getInt(0);
                    String l_name = cursor.getString(1);
                    String name = cursor.getString(2);
                    String num = cursor.getString(3);
                    String email = cursor.getString(4);
                    String address = cursor.getString(5);
                    String photo = cursor.getString(6);
                    String sms = cursor.getString(7);

                    res.add(new Contact(id, l_name, name, num, address, email, photo, sms));

                } while (cursor.moveToNext());
            }

        return res;
    }

    public long addNewContact(Contact contact) {
        ContentValues values = new ContentValues();

        values.put(LAST_NAME, contact.get_last_name());
        values.put(NAME, contact.get_name());
        values.put(NUMBER, contact.get_number());
        values.put(EMAIL, contact.get_email());
        values.put(ADDRESS, contact.get_adress());
        values.put(PHOTO, contact.get_photo());

        long id = _db.insert(TABLE, null, values);
        contact.set_id(id);

        return id;
    }

    public void removeContact(Contact contact) {
        _db.delete(TABLE, ID + "=" + contact.get_id(), null);
    }

    public void updateContact(Contact contact) {
        ContentValues values = new ContentValues();

        values.put(LAST_NAME, contact.get_last_name());
        values.put(NAME, contact.get_name());
        values.put(NUMBER, contact.get_number());
        values.put(EMAIL, contact.get_email());
        values.put(ADDRESS, contact.get_adress());
        values.put(PHOTO, contact.get_photo());
        values.put(SMS, contact.get_sms());

        _db.update(TABLE, values,"id=" + contact.get_id(), null);
    }

    public List<Contact> search(String text) {
        List<Contact> data = getContacts();

        List<Contact> res = new ArrayList<Contact>();

        for (Contact contact : data) {
            if (contact.get_last_name().toLowerCase().contains(text.toLowerCase())) {
                res.add(contact);
            } else if (contact.get_name().toLowerCase().contains(text.toLowerCase())) {
                res.add(contact);
            }
        }

        return  res;
    }

    public Contact getContactById(long id) {
        Contact contact = null;

        final String query = "SELECT * FROM " + TABLE + " WHERE " + ID +"=" + Long.toString(id) + ";";
        Cursor cursor = _db.rawQuery(query, null);

        if (cursor != null && cursor.moveToFirst()) {
            String l_name = cursor.getString(1);
            String name = cursor.getString(2);
            String num = cursor.getString(3);
            String email = cursor.getString(4);
            String address = cursor.getString(5);
            String photo = cursor.getString(6);
            String sms = cursor.getString(7);

            contact = new Contact(id, l_name, name, num, address, email, photo, sms);
        }

        return contact;
    }
}
